import {Component, HostListener, OnInit} from '@angular/core';
import {PubNubAngular} from 'pubnub-angular2';
import {UserService} from './services/user.service';
import {FormControl, Validators} from '@angular/forms';
import {config} from '../config';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  channel = 'public';
  user = '';
  messages: any[] = [];
  userMessage = new FormControl('', [Validators.required]);
  location: any;
  markers: any[] = [];
  lastUser = '';

  @HostListener('window:beforeunload', ['$event'])
  beforeunloadHandler(event) {
    this.unsubscribe();
  }

  constructor (private pubnub: PubNubAngular,
               private userService: UserService) {
  }

  ngOnInit () {
    this.user = this.userService.generateNickname();

    this.pubnub.init({
      publishKey: config.pubnub.publishKey,
      subscribeKey: config.pubnub.subscribeKey
    });

    this.pubnub.setUUID(this.user);

    if (window.navigator && window.navigator.geolocation) {
      navigator.geolocation.getCurrentPosition(position => {
        this.location = position.coords;
      });
    };

    this.pubnub.subscribe({channels: ['public'], triggerEvents: true, withPresence: true});

    this.pubnub.getMessage('public', (res) => {
      const data = res.message;
      this.messages.push(data);
      this.lastUser = data.user;
      if (data.latitude && data.longitude) {
        this.markersControl(data);
      }
    });

    setInterval(() => this.checkUsers(), 5000);

  }

  addMessage() {
    const data = {
      user: this.user,
      text: this.userMessage.value,
      latitude: this.location['latitude'],
      longitude: this.location['longitude']
    };

    if (location) {
      data['latitude'] = this.location['latitude'];
      data['longitude'] = this.location['longitude'];
    }

    this.pubnub.publish(
      {
        message: data,
        channel: 'public'
      }, (status) => {
        if (status.error) {
        } else {
          this.userMessage.reset();
        }
      }
    );
  }

  markersControl(data) {
    let isNew = true;
    this.markers.forEach((m) => {
      if (m.user === data.user) {
        m = data;
        isNew = false;
      }
    });

    if (isNew) {
      this.markers.push(data);
    }
  }

  checkUsers() {
    this.pubnub.hereNow(
      {
        channels: ['public'],
        includeUUIDs: true
      },
      (satatus, response) => {
        const online = response.channels[this.channel].occupants.map((item) => item.uuid);
        this.markers = this.markers.filter((m) => online.includes(m.user));
      }
    );
  }

  setIconColor(user) {
    return user === this.lastUser ? 'http://maps.google.com/mapfiles/marker.png' : 'http://maps.google.com/mapfiles/marker_yellow.png';
  }

  unsubscribe() {
    this.pubnub.unsubscribe({channels : ['public']});
  }
}
