import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { PubNubAngular } from 'pubnub-angular2';
import { AppComponent } from './app.component';
import { UserService } from './services/user.service';
import { ReactiveFormsModule } from '@angular/forms';
import { AgmCoreModule } from '@agm/core';
import {config} from '../config';

@NgModule({
  declarations: [
    AppComponent,
  ],
  imports: [
    BrowserModule,
    ReactiveFormsModule,
    AgmCoreModule,
    AgmCoreModule.forRoot({
      // please get your own API key here:
      // https://developers.google.com/maps/documentation/javascript/get-api-key?hl=en
      apiKey: config.google.API_KEY
    })
  ],
  providers: [
    PubNubAngular,
    UserService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
